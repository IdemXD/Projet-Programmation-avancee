#ifndef CONSTANTES_H
#define CONSTANTES_H

/**
	*\file constantes.h
	*\author Medi Boudhane
	*\version 1.0
*/

/**
 * \brief Nombre de personnages joués
 */
#define NB_PERSONNAGES 2

/**
 * \brief Le coté du plateau
*/
#define TAILLE_PL 5

/**
 * \brief Tableau des char de toutes les salles du jeu
*/
// extern const char LETTRES_SALLES[12] ;

static const char LETTRES_SALLES[12] = {'R','V','C','O','S','E','F','M','D','N','T','X'} ;

/**
 *\ brief Taille d'un sprite
*/
#define TAILLE_S 120

#endif
